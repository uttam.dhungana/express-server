var express = require('express');
var http = require('http');
var app = express();

var user = require('./client/user_operation');
app.set('port', process.env.PORT || 3000);

app.use(express.json());

app.get('/api/user_list', user.getUsers);
app.get('/api/get_user/:id', user.getUser);
app.post('/api/add_user', user.saveUser);
app.put('/api/edit_user/:id', user.editUser);
app.delete('/api/delete_user/:id', user.deleteUser);

http.createServer(app).listen(app.get('port')); 

console.log('server running at: http://localhost:' + app.get('port'))