const client = require('./client');

exports.getUsers = function (req, res) {
    client.List(req.params, (error, users) => {
        if (error) throw error;

        res.send(users);
    });
}

exports.getUser = function (req, res) {
    client.GetUser(req.params, (error, users) => {
        console.log(users);
        if (error) throw error;

        res.send(users);
    });
}

exports.saveUser = function (req, res) {
    console.log(req.body);
    client.insert(req.body, (error, users) => {
        console.log(users);
        if (error) throw error;

        res.send(users);
    });    
}

exports.editUser = function (req, res) {
    client.update(req.body, (error, users) => {
        console.log(users);
        if (error) throw error;

        res.send(users);
    });
}

exports.deleteUser = function (req, res) {
    client.delete(req.params, (error, users) => {
        console.log(users);
        if (error) throw error;

        res.send(users);
    });
}